package com.example.demobeanlifecycle;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class EventListenerExample {

    @EventListener
    public void onApplicationEvent(MyEvent event) {
        System.out.println("Event list initialized" +event.getSource().getClass());
    }
}
