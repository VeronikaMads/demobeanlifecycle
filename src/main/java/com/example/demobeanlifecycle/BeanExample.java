package com.example.demobeanlifecycle;

import com.example.util.UtilBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
@ConditionalOnBean(EventListenerExample.class)
@Component
public class BeanExample implements InitializingBean {
    @Autowired
    private Environment environment;
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private UtilBean bean;
    @Value("${some.property}")
    private String myProperty;
    @Value("${another.property:nika}")
    private String anotherProperty;

    public BeanExample() {
        System.out.println("Construct call");
    }

    @PostConstruct
    public void init() {
        System.out.println("PostConstruct" + Arrays.toString(environment.getActiveProfiles()));
        System.out.println("My property" +myProperty);
        System.out.println("My property" +anotherProperty);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("AfterPropertiesSet" + Arrays.toString(environment.getActiveProfiles()));
        publisher.publishEvent(new MyEvent(this));

    }
}
