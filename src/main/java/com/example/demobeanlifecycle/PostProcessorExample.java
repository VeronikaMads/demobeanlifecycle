package com.example.demobeanlifecycle;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class PostProcessorExample implements BeanPostProcessor {
    @Autowired
    private Environment environment;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
//        if (bean instanceof BeanExample) {
//            System.out.println("Before init" + Arrays.toString(environment.getActiveProfiles()));
            System.out.println("Before init" + beanName);
//        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof BeanExample){
            System.out.println("After init " +beanName);}
        return bean;
    }
}
