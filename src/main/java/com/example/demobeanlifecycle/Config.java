package com.example.demobeanlifecycle;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
@ComponentScan(basePackages = {"com.example.util", "com.example.demobeanlifecycle"})
@Configuration
public class Config {
}
