package com.example.demobeanlifecycle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "com.example.util")
@SpringBootApplication
public class DemoBeanLifeCycleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoBeanLifeCycleApplication.class, args);
    }

}
